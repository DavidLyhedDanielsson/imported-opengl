#version 450 core

#include "commonIncludes.glsl"

layout(location = 0, rgba8) uniform image2DMS inImage;
layout(location = 1) uniform sampler2DMS depthBuffer;
//layout(location = 1, rgba8) uniform image2DMS outImage;

uniform mat4 viewMatrix;
uniform mat4 projectionInverseMatrix;

layout(std140) buffer ScreenSize
{
    int screenWidth;
    int screenHeight;
};

shared int lightCount;
shared int localLightIndices[MAX_LIGHTS_PER_TILE];

const int PLANE_TOP = 0;
const int PLANE_BOTTOM = 1;
const int PLANE_LEFT = 2;
const int PLANE_RIGHT = 3;

vec4 CreatePlane(vec3 far0, vec3 far1)
{
    vec3 planeABC;
    planeABC = normalize(cross(far0, far1));
    float dist = 0.0f; //dot(far0, planeABC);

    return vec4(planeABC, dist);
}

void AddLightIfPossible(int lightIndex)
{
    int index = atomicAdd(lightCount, 1);

    if(index < MAX_LIGHTS_PER_TILE)
        localLightIndices[index] = lightIndex;
}

const int BOTTOM_LEFT = 0;
const int BOTTOM_RIGHT = 1;
const int TOP_RIGHT = 2;
const int TOP_LEFT = 3;
const int CENTER = 4;

vec3[5] CreateFarPoints()
{
    const vec2 offsets[5] =
    {
        vec2(0.0f, 0.0f)
        , vec2(1.0f, 0.0f)
        , vec2(1.0f, 1.0f)
        , vec2(0.0f, 1.0f)
        , vec2(0.5f, 0.5f)
    };

    vec3 viewPositions[5];

    for(int j = 0; j < 5; ++j)
    {
        vec3 ndcPosition = vec3(((vec2(gl_WorkGroupID.xy) + offsets[j])
                                        * vec2(WORK_GROUP_WIDTH, WORK_GROUP_HEIGHT))
                                        / vec2(screenWidth, screenHeight), 1.0f);
        ndcPosition.x *= 2.0f;
        ndcPosition.x -= 1.0f;
        ndcPosition.y *= 2.0f;
        ndcPosition.y -= 1.0f;

        vec4 unprojectedPosition = projectionInverseMatrix * vec4(ndcPosition, 1.0f);
        unprojectedPosition /= unprojectedPosition.w;

        viewPositions[j] = vec3(unprojectedPosition);
    }

    return viewPositions;
}

vec4[4] CreatePlanes(vec3 viewPositions[5])
{
    vec4 planes[4];

    planes[PLANE_TOP] = CreatePlane(viewPositions[TOP_RIGHT], viewPositions[TOP_LEFT]);
    planes[PLANE_BOTTOM] = CreatePlane(viewPositions[BOTTOM_LEFT], viewPositions[BOTTOM_RIGHT]);
    planes[PLANE_RIGHT] = CreatePlane(viewPositions[BOTTOM_RIGHT], viewPositions[TOP_RIGHT]);
    planes[PLANE_LEFT] = CreatePlane(viewPositions[TOP_LEFT], viewPositions[BOTTOM_LEFT]);

    return planes;
}

layout(local_size_x = WORK_GROUP_WIDTH, local_size_y = WORK_GROUP_HEIGHT) in;
void main()
{
    if(gl_LocalInvocationIndex == 0)
        lightCount = 0;

    barrier();

    for(int i = int(gl_LocalInvocationIndex); i < int(lights.length()); i += int(gl_WorkGroupSize.x * gl_WorkGroupSize.y))
    {
        LightData light = lights[i];

        vec3 viewPositions[5] = CreateFarPoints();
        vec4 planes[4] = CreatePlanes(viewPositions);

        vec3 zeroPos = vec3(viewMatrix * vec4(light.position, 1.0f));

        if(dot(zeroPos, zeroPos) > light.strength * light.strength)
        {
            bool inside = false;

            vec3 planeForward = normalize(viewPositions[CENTER]);
            if(dot(normalize(zeroPos), planeForward) > 0.0f)
            {
                inside = true;

                for(int j = 0; j < 4; ++j)
                {
                    float dist = dot(zeroPos, vec3(planes[j])) + planes[j].w;
                    if(dist < -light.strength)
                    {
                        inside = false;
                        break;
                    }
                }
            }

            if(inside)
                AddLightIfPossible(i);
        }
        else
            AddLightIfPossible(i);
    }

    barrier();

    if(gl_LocalInvocationIndex == 0)
    {
        int arrayIndex = GetArrayIndex(gl_WorkGroupID.xy);
        int startIndex = arrayIndex * MAX_LIGHTS_PER_TILE;

        int cappedLightCount = min(lightCount, MAX_LIGHTS_PER_TILE);
        for(int i = 0; i < cappedLightCount; ++i)
            lightIndices[startIndex + i] = localLightIndices[i];
        for(int i = cappedLightCount; i < MAX_LIGHTS_PER_TILE; ++i)
            lightIndices[startIndex + i] = -1;

        TileLightData data;
        data.start = startIndex;
        data.numberOfLights = lightCount;
        data.padding = ivec2(gl_GlobalInvocationID);

        tileLightData[arrayIndex] = data;
    }
}